Mohamed A. Belakhoua mab21
# Lab Notebook

## Meeting Notes

# Lab Notebook : Mohamed A. Belakhoua : mab21
- This is the laboratory notebook of Mohamed A. Belakhoua

# Meeting Notes
### 2/2/2023 - Zoom meeting
- choose processor (AT ASM D21 E) https://www.tme.eu/Document/7c5e6710b51fca5457483ef02a4076ce/SAMD21-DTE.pdf
- and LED (0603R) https://www.mouser.com/datasheet/2/143/everlight_fairs18742-1-1734971.pdf
- We need to deliver 200J/cm^2 per PDT

### 2/5/2023 - Grainger Library
- work on proposal and block diagram
- Reading different datasheets and making sure the parts we are buying are aligned with what we are looking for from the design.

### 2/9/2023 - Zoom meeting
- Brainstormed testing options. 
- Testing the effect of light on brain tissue.
- We talked about the different options we have to communicate wirelessly to our device (RFID as of now)

### 2/16/2023 - Zoom meeting
- Demonstrated my initial circuit desing with a schematic sheet (The initial schematic sheet is provided in the project proposal) 
- I used Altium designer as CAD tool to design the circuit
- I suggested moving away from RFID to Bluetooth for multiple reasons (Reliability, Security, Safety)
### 2/27/2023 - Design Review
- Attended the design review to discuss the Schematic design.
- Connecting the LED to a LED driver instead of directly to MCU to deliver more current to the LED, thus more power delivered to the Brain tissue.
- Finilized the Design for the schematic sheet 
- Decided to use Cree LEDs as they deliver alot of power comparing to other LEDs
### 3/2/2023 - Zoom meeting
- Testing proposal done by sunday evening.
- Finished the Design of the PCB.
- Passing the Audit of PCBWay
### 3/7/2023 - ECEB (TA Meeting)
- Jack will send STL prints and then Al will do FDM prints so Jack can send things to machine shop (alumninum)
### 3/9/2023 - Zoom meeting
- The meetings are becoming more of a touch-base with our team members to make sure we were working to the timeline we set for ourselves.

### 3/21/2023 - ECEB (TA Meeting)
- Soldering and Troubleshoothing the PCB
- We soon realized that we have not ordered enough parts.
- We proceeded by ordering 5 more for each part
### 3/23/2023 - Zoom meeting
- Still working on the Soldering and troubleshooting the Desing
- Having hard  time soldering the PCB, since we are using very small parts and our MCU was a QFN packaging
### 3/28/2023 - ECEB (TA Meeting)
- After going through many iterations of soldering, finally I passed the continuty test.
- Now moved to uploading the code to our MCU but failing
- After more troubleshooting, came to notice that our Boot pin of the MCU was not grounded. The Boot pin was floating.
- I fixed the issue by soldering a very thin wire from the pin of the MCU to the nearest ground.
- That fixed we can see desired signals coming in and out from the MCU by using the Osciloscope.

### 3/30/2023 - Zoom meeting
- Touch-base regarding status of our project and how we see the rest of the semester going
- Still working on the PCB 
- The LED still not shining Light

### 4/5/2023 - Al's House (Team Meeting)
- Still working on troubleshooting the PCB.

### 4/8/2023 - Zoom meeting
- At the same time I am working on implemetning the software by using Dev Board that I have borrowed from a friend. 
- The dev board has the same specs as our design

### 4/11/2023 - ECEB (TA Meeting)
- These meetings at this point became mostly to make sure we were on thr right track.
- Kept troubleshting the PCB

### 4/13/2023 - Zoom meeting
- Demonstrated to the team what I have been working in general.
- Working on our final Demo

### 4/18/2023 - ECEB (TA Meeting)
- Kept troubleshooting the PCB
- Got ready for the Final Demo
- This was considered the mock Demo


### 4/22/2023 - Al's House (Team Meeting)
- Came to notice that I had a mistake in the design of the LED driver.
- Fixed it. Boooooooom! the LED is shining Light as desired.
- Joined the team at Al's house.
- Started working on the Program and connecting the Device through Bluetooth

### 4/27/2023 - Zoom meeting
- I suggested to come up with a second revision that will have smaller dimesnions.
- Started working on the new Revision
- Finished the new PCB by the end of the week and got it ordered

### 4/25/2023 - ECEB (Final Demo)
- Recieved the Second revision of the PCB and got the Bluetooth working!

### 4/25/2023 - ECEB (Final Demo)
- FINAL DEMO!!!!!

### 5/2/2023 - ECEB (Final Presentation)
- FINAL PRESENTATION!!!!
