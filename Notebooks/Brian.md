# Lab Notebook
Brian Diner bdiner2

### Meeting Notes

#### 2/2/2023, Remote

team meeting

#### 2/5/2023, Grainger Library

work on proposal and block diagram

choose processor (AT ASM D21 E) https://www.tme.eu/Document/7c5e6710b51fca5457483ef02a4076ce/SAMD21-DTE.pdf

and LED (0603R) https://www.mouser.com/datasheet/2/143/everlight_fairs18742-1-1734971.pdf

begin research into power draw for led and processor

need 200J/cm^2 per PDT, 0603 gives .11 lumens at 20mA meaning it takes 1/5 of sec for full PDT 

    is this correct? 
    need to look into luminosity physics

avg 30g battery has 450mAh so power draw not likely to be huge concern

Jack has block diagram on his computer 

Feb 2 2023 Meeting
Deliverables by the end of the term
Proper wavelength LED with diffuser
Programmable LED 
Does not need to be medical grade titanium (can be a 3D printed casing)

### meeting minutes with full beacon group
Thermal energy production discussion 
To do (anant and emily): how to measure energy production, limits for tissue, etc
May need to deliver in bursts to limit heat generation

testing equipment brainstorming:
thermistors
energy measurement panel (like a camera sensor with the ability to see wavelength)
3d printed box/enclosure

Battery 
Ideally will last 5 years - if not, we need to find a recharging mechanism 
Examples: RNS device
Need to calculate the amperage needed to run the PDT device to figure out how long a battery will last / which battery we should choose. 

MRI compatible
That is a later Phase 2 consideration 
And definitely required if we want to take this to market

Fixation of the device to the scaffold
Need to make sure that the device stays in place and doesn't jostle around 
Could create a tent-pin system or some sort of screw mechanism

They have machining capabilities within ECE

Al showed video of the initial prototype and the STL files 


Anant and emily to do - work on ordering access
Al - will share the STL files and can meet to share device 

Final proposal due next Th - anant and emily are happy to review

March 2, 2023
Thermal testing - create a model for heating and measuring that 
Thermistors - can put temp sensor on PCB
Optical sensors 
https://pubmed.ncbi.nlm.nih.gov/26146557/ - recipe for gelatin thing for representing brain 
Implanting sensors - sensor on a needle set up?

Cree LEDS? Highest intensity in energy - https://www.cree-led.com/products/leds/hb/smd-single/

They gave presentations to professor and TA
May not be able to work directly with cadavers

Jack will send STL prints and then Al will do FDM prints so Jack can send things to machine shop (alumninum) - 

PCB ordering next week. 

Bluetooth and not RFID
More reliable, efficient, and secure, makes PCB smaller
GUI for interface with graphs etc

GPIO and raspberry pi if needed for tight deadlines
If we want to order outside UIUC https://www.pcbway.com/

2 week lead time for cadaver from science care for Champaign

End of this weekend - have methods written out for cadaver trial
Testing for hydrogel testing
Papers for optical analysis

ORdering cadavers
Check in neurosurg is ordering any
500-1000$ per brain and $3000ish for shipping
Ordering in bulk would greatly reduce shipping costs

Dates and times and location of study
Basic outline of what study will look like
Reach out to Harley lab for hydrogel 

March 9 2023
Thermister for the phantom
https://www.omega.com/en-us/temperature-measurement/temperature-wire-sensors/th-44000/p/TH-44006-120-T?gclsrc=aw.ds&gclid=Cj0KCQiApKagBhC1ARIsAFc7Mc54zG6xllHMQsr8AynOCCQd5nGrzE1nT5lRYnimobGjfu0GwdYtxXYaAkUDEALw_wcB 
Photodectector
https://www.amazon.com/BOJACK-Photoresistance-Sensitive-Resistor-GM5539/dp/B07TQMJ212/ref=sr_1_4?keywords=Photoresistor&qid=1678405363&sr=8-4
Pair of the two at each given depth 
Breadboard this apparatus thing

March 30, 2023
Early next week - PCB revision board
Device prototype
PCB can fit housing without the connector
Lens? - have one but need to redo math to ensure proper diffusion - need to finalize this  
Testing apparatus
Board for testing apparatus wired up by end of next week
Cadaver testing - too late to get fresh cadaver head, but there are heads in anatomy lab for us to use, IRB already in place for capstone

#### calculations i did on my own time

Assumptions: 

We use the following assumptions to determine the optical power of our LED and the energy dose it can deliver to a cranial cavity. 

From the datasheet we know our red LED emits a wavelength of 630 nm at a luminous intensity,  Iv, between 450 and 1800 mcd, controllable by forward current [1]
Assuming a sufficiently narrow emission wavelength we can say the relationship between photometric units, related to perceived eyeball sensitivity, and radiant units, related to arbitrary photon energy, can be calculated with one conversion factor: luminous efficacy, K [2]. We find this luminous efficacy as a function of luminous efficiency provided by the RCA Electro-Optics Handbook [3]. 

Therefore we can say radiant intensity Ie= IvV()*673 = Iv178.345 [W/sr]

For the ease of calculation we will assume the tumor cavity is a spherical cap, with a surface area of 50cm2 at a distance of 1 cm from the lambertian light source. The radius of this spherical cap is related to the distance of the source by A=2rh, where h is the distance from the source. And the 2D angle is related to the radius by A=2r2(1-cos()). Solving for the interested quantities we find:
r=A2h=502(1)=7.95cm
Plugging r into the second area formula: 
A=2r2(1-cos()) 
=cos-1(1-A2r2)=29.0°

We can now use this 2D angle to find the solid angle Ω, that contains the 3D portion of light that will reach the tumor cavity. 
Ω=2(1-cos()=0.787 sr
Using the solid angle we can relate radiant intensity to radiant flux in terms of watts, and to irradiance in terms of W/m2.
Radiant flux, e=Ie  [W] (~2-8mW)
Irradiance, Ee=Ie A [W/m2].
Using the optical intensity from the data sheet we can thus say our tumor cavity can receive 39.35 to 158.8 uW/cm2 from our LED.  We can use this quantity to determine the fluence, or dose delivered to the cavity from the relating watts to joules per second. We stated our dose should be 200 J/cm2. Thus, our device must deliver the stated irradiance for 350 to 1400 hours to deliver the required effective photodynamic therapy dose. This is a huge amount of time but it makes sense. Most PDT therapies are delivered with much more powerful devices 

If we assume a 12hour requirement to avoid oxygen depletion and to maintain 5-ALA saturation we can deliver 6J/cm2 which is above the LD50 for Glioma cells [4]. Adding more LEDs to increase the intensity is the next logical step to engineer a device that can function clinically.

Further calculations should complicate the LED as a light source and the tumor cavity as a surface.
Penetration Depth
[6]
Assuming absorption and scattering coefficients of .2 and 20 we get a penetration depth of 3.45cm. This means we have 23% of original intensity at 5mm. This could be therapeutic. Another source that listed the following coefficients. They look less promising.
[7]
These properties result in calculated optical penetration of CSF = 286mm, GM= 1.3mm, WM = .31mm. This results in 2% penetration and 0% penetration at 5mm in GM and WM respectively. Therefore, though we could deliver 25J/cm^2 at this depth with a better device, it would likely require an enormous intensity that would risk photobleaching the tissue. [8]

Fluence plotted by depth in a brain phantom including scalp and skull [7]

Battery

Assuming a 450mAh battery and a draw of <1mAh (device off) 40mA (device on, low intensity), 120mA (device on, high intensity) the battery will drain in about 11 hours of on time at low intensity and 3.75 hours at high intensity. This indicates our battery will not be able to support a full length PDT session. A larger battery will be cumbersome and will likely not fit in the footprint. Unclear whether a single use battery is the best solution. May need to return to the drawing board and include a charging mechanism or something like near field excitation of the device.

Lens

Our current device’s housing opening is wide, close to the tumor cavity, and the LED wavelength band is narrow enough for light loss to not really be a concern. If we wish to shrink the aperture opening in the future any lens that focuses light such as a plano concave type shape will benefit the device by maximizing and standardizing the illumination given to the cavity. A lens capable of providing a focal length of 10mm will need a concave lens with half that value as its radius. This may prove very difficult to manufacture ourselves. The current dummy lens will not focus any of our LED light. It is in fact placed optically backwards.
Good focal length calculator[10].

Pioneer Optics Company specializes in manufacturing optical diffusers for PDT. 
Here’s a cool pic of the kind of work they do with microlens and optical fibers that may be of interest.

Sources
[1]https://assets.cree-led.com/a/ds/h/HB-CLM1B-RKW-AKW.pdf
[2]https://www.jensign.com/LEDIntensity/#:~:text=Calculate%20the%20radiant%20intensity%20I,200%20%3D%2025%20mW%2Fsr.
[3] https://www.ok1rr.com/tubes/burle/Electro-Optics-Handbook.pdf
[4] https://opg.optica.org/directpdfaccess/3f33ef52-a2d2-4966-967740f4385a0e49_311897/boe-6-3-770.pdf?da=1&id=311897&seq=0&mobile=no
[5] https://internationallight.com/measurement-geometries-chapter-7-light-measurement-tutorial
[6]https://www.spiedigitallibrary.org/journals/journal-of-biomedical-optics/volume-20/issue-12/125001/Optical-properties-of-tumor-tissues-grown-on-the-chorioallantoic-membrane/10.1117/1.JBO.20.12.125001.full?SSO=1
[7] https://onlinelibrary.wiley.com/doi/10.1002/jbio.201800173
[8] https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6985206/
[9] https://onlinelibrary.wiley.com/doi/full/10.1111/php.13219
[10] https://www.edmundoptics.com/knowledge-center/tech-tools/focal-length/

https://opg.optica.org/boe/fulltext.cfm?uri=boe-14-2-714&id=524876###
NEW: upgrades in open source interstitial PDT treatment software PDT-SPACE to better optimize light source placement and avoid overdosage of surrounding region
Further research into this tool would be valuable because PDT dosimetry is a hard problem to solve analytically
https://gitlab.com/FullMonte/pdt-space/-/wikis/Running-PDT-SPACE



