# Lab Notebook : Jack Stender : Stender3
- This is the laboratory notebook for Jack Raymond Stender pertaining to his ECE445 project.
# Meeting Notes
### 2/2/2023 - Zoom (Team meeting)
- Deliverables by the end of the term
    - Proper wavelength LED with diffuser
    - Programmable LED 
    - Does not need to be medical grade titanium (can be a 3D printed casing)
- Thermal energy production discussion 
    - To do (anant and emily): how to measure energy production, limits for tissue, etc
    - May need to deliver in bursts to limit heat generation
- testing equipment brainstorming:
    - thermistors
    - energy measurement panel (like a camera sensor with the ability to see wavelength)
    - 3d printed box/enclosure
- Battery 
    - Ideally will last 5 years - if not, we need to find a recharging mechanism 
    - Examples: RNS device
    - Need to calculate the amperage needed to run the PDT device to figure out how long a battery will last / which battery we should choose. 
- MRI compatible
    - That is a later Phase 2 consideration 
    - And definitely required if we want to take this to market
- Fixation of the device to the scaffold
    - Need to make sure that the device stays in place and doesn't jostle around 
    - Could create a tent-pin system or some sort of screw mechanism
### 2/5/2023 - Grainger Library
- Worked on project proposal. Made the first draft of the block diagram and imported visual aids to the document. Wrote and formatted the introduction. We, as a group, brainstomed and troubleshooted potential pitfalls in our design process as well as the project itself. We spent considerable time reading datasheets and collecting specific values for our calculations.
### 2/9/2023 - Zoom (Team meeting)
- Brainstormed testing options. Discussed at length about using balistics gel to measure energy falloff inside the brain tissue.
- Testing the effect of light on brain tissue would be potentially groundbreaking.
- We also talked about potentially using bluetooth and whether or not its damaging to the brain.
- Went over finished proposal before submission.
- Does RFID have a higher propensity for brain tumors?
- We braistomed names as well... trying to get everything in order
### 2/16/2023 - Zoom (Team meeting)
- Demonstrated my first model for the exterior housing of the implant
- worked on tweaking the implant and makeing it work better for our needs
- discussed at length the testing aparatus and how we want to go about working on the rest of the project.
### 2/16/2023 - Zoom (TA meeting)
- Add a section talking about the 5 - ALA and the way that it interacts with the cancer cells.
- Elaborate on the subsystem requirements. 
### 3/2/2023 - Zoom (Team meeting)
- Testing proposal done by sunday evening
- Handled some beurocratic stuff and got ready to order the PCB.
### 3/7/2023 - ECEB (TA Meeting)
- Finalized housing design and prepared files for the Machine Shop.
- Got organized and began pivoting to the testing apparatus.
### 3/9/2023 - Zoom (Team Meeting)
- At this point, these meetings became more of a touch-base to make sure we were working to the timeline we set for ourselves.
- Troubleshooted a few small details regarding the PCB
- finalized the design parameters for the testing aparatus and how we want it to function overall.
### 3/14/2023 - ECEB (TA Meeting)
- Presented my first design for the testing apparatus.
- Handled some beurocratic stuff and organized the work we had done thus far.
- Ordered PCB 
### 3/16/2023 - Zoom (Team Meeting)
- Touch-base regarding COZAD, OTM proposal, and other smaller details ancillary to the project itself
### 3/21/2023 - ECEB (TA Meeting)
- Began working on the PCB
- Realized we should have bought way more components because we messed up the first time
- Ordered new componenets for PCBs
### 3/23/2023 - Zoom (Team Meeting)
- Touch-base regarding status of our project and how we see the rest of the semester going
- Ordered components for the testing apparatus
### 3/28/2023 - ECEB (TA Meeting)
- Continued to troubleshoot PCB
- Organized our information
- Redesigned testing apparatus to more effectively meet our goals
### 3/30/2023 - Zoom (Team Meeting)
- Touch-base regarding status of our project and how we see the rest of the semester going
- got feeedback on the testing apparatus redesign
### 4/5/2023 - Al's House (Team Meeting)
- Finished getting the sensors ready for the testing apparatus
- Did one final redesign and got it printed
### 4/6/2023 - Al's House (One on One Meeting)
- Worked overnight on the testing apparatus and began wiring everything together
- Designed stadoff for the rig to allow us to inset the housing
### 4/8/2023 - Zoom (Small Group Meeting)
- Got some renderings done regarding COZAD and other small beurocratic stuff
### 4/11/2023 - ECEB (TA Meeting)
- These meetings at this point became mostly to make sure we were on thr right track.
- Ketpt troubleshting the PCB
- It is important to note here that Sarath was instrumental in keeping us on track. He made sure we never missed a deadline and kepts us organnized and honest. I'm not sure we would have been so successful without his help. 
### 4/13/2023 - Zoom (Team Meeting)
- Touch base and showed what we've been working on.
- Got ready for our final Demo
### 4/18/2023 - ECEB (TA Meeting)
- Kept troubleshooting the PCB
- Got ready for the Final Demo
- This was considered the mock Demo
### 4/20/2023 - Zoom (Team Meeting)
- Communicated to the team how everything was going and got ready for the final Demo
### 4/22/2023 - ECEB (Team Meeting)
- Kept troubleshooting the PCB, finally got it to work!!!!
### 4/22/2023 - Al's House (One on One Meeting)
- Finished building the testing rig and got the data to display nicely
### 4/23/2023 - ECEB (One on One Meeting with Al)
- touched base regarding some ancillary little details and got ready for the final Demo
### 4/24/2023 - Al's House (One on One Meeting)
- Picked up the testing apparatus and made sure everything was working properly.
### 4/25/2023 - ECEB (Final Demo)
- FINAL DEMO!!!!!
### 4/27/2023 - ECEB (Team Meeting)
- Touched base with the team, organized info for the final Presentation
### 5/2/2023 - ECEB (Final Presentation)
- FINAL PRESENTATION!!!!
